# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext as _
import datetime

from apps.johnson.models import PlannedResearch


class PlannedResearchForm(forms.ModelForm):
    class Meta:
        model = PlannedResearch

    year = forms.IntegerField(initial=datetime.date.today().year+1, widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    def __init__(self, *args, **kwargs):
        super(PlannedResearchForm, self).__init__(*args, **kwargs)
        queryset = PlannedResearch.objects.filter(year=datetime.date.today().year)
        self.fields['continued_research'] = forms.ModelChoiceField(queryset=queryset, required=False)

    def clean(self):
        cleaned_data = super(PlannedResearchForm, self).clean()

        # Skopiowania nazwy zadania z kontynuowanego zadania
        continued_research = cleaned_data.get('continued_research', None)
        name = cleaned_data.get('name', '')
        if continued_research:
            if name:
                raise forms.ValidationError(_(u'If research is continued field name should remain empty'))
            else:
                cleaned_data['name'] = continued_research.name
        else:
            if not name:
                raise forms.ValidationError(_(u'Field name or continued research name is required'))

        # Sprawdzenie czy liczba słów w polach: nazwa, cele i efekty nie przekracza 300 słów
        name = cleaned_data.get('name', '')
        goal = cleaned_data.get('goal', '')
        effects = cleaned_data.get('effects', '')

        name_words = len(name.split())
        goal_words = len(goal.split())
        effects_words = len(effects.split())

        if name_words + goal_words + effects_words >= 300:
            raise forms.ValidationError(
                _("Total number of words in the fields: name %(name_words)s, goal (%(gaol_words)s) and effects (%(effects_words)s) exceed 300 words." % {'name_words': name_words, 'goal_words': goal_words, 'effects_words': effects_words})
            )

        return cleaned_data

    def clean_year(self):
        data = self.cleaned_data['year']
        if data != datetime.date.today().year+1:
            raise forms.ValidationError(_(u'Please specify a next year for planned research.'))
        return data