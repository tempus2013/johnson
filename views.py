# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext

from apps.johnson.forms import PlannedResearchForm
from apps.johnson.models import PlannedResearch

TEMPLATE_ROOT = 'johnson/'


def index(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'index.html', kwargs, context_instance=RequestContext(request))


# -----------------------------------------------------------------------------------------
# --- PLANS
# -----------------------------------------------------------------------------------------

def plans(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'plans.html', kwargs, context_instance=RequestContext(request))


def plans_staff(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'plans_staff.html', kwargs, context_instance=RequestContext(request))


def plans_research_add(request):
    form = PlannedResearchForm(request.POST or None)

    if request.POST:
        if form.is_valid():
            planned_research = form.save()
            messages.success(request, _(u'Form was successfully saved'))
            if 'save' in request.POST:
                return redirect('apps.johnson.views.plans')
            else:
                return redirect(reverse('apps.johnson.views.plans_research_edit', kwargs={'research_id': planned_research.id}))
        else:
            messages.error(request, _(u'Correct errors in the form'))

    kwargs = {'form': form}
    return render_to_response(TEMPLATE_ROOT+'plans_research_add.html', kwargs, context_instance=RequestContext(request))


def plans_research_edit(request, research_id):
    planned_research = get_object_or_404(PlannedResearch, id=research_id)
    form = PlannedResearchForm(request.POST or None, instance=planned_research)

    if request.POST:
        if form.is_valid():
            planned_research = form.save()
            messages.success(request, _(u'Form was successfully saved'))
            if 'save' in request.POST:
                return redirect('apps.johnson.views.plans')
            else:
                return redirect(reverse('apps.johnson.views.plans_research_edit', kwargs={'research_id': planned_research.id}))
        else:
            messages.error(request, _(u'Correct errors in the form'))

    kwargs = {'form': form}
    return render_to_response(TEMPLATE_ROOT+'plans_research_add.html', kwargs, context_instance=RequestContext(request))


def plans_srd(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'plans_srd.html', kwargs, context_instance=RequestContext(request))

# -----------------------------------------------------------------------------------------
# --- REPORTS
# -----------------------------------------------------------------------------------------

def reports(request):
    kwargs = {}
    return render_to_response(TEMPLATE_ROOT+'reports.html', kwargs, context_instance=RequestContext(request))


