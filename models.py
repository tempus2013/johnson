from django.db import models
from django.utils.translation import ugettext as _
from apps.trainman.models import Department


class PlannedResearch(models.Model):
    continued_research = models.ForeignKey('PlannedResearch', null=True, blank=True)
    name = models.CharField(max_length=256, help_text=_(u'Leave empty if research is continued'), null=True, blank=True)
    goal = models.TextField()
    effects = models.TextField()
    year = models.PositiveIntegerField()
    unit = models.ForeignKey(Department)

    def __unicode__(self):
        return self.name