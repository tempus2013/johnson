# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PlannedResearch'
        db.create_table('johnson_plannedresearch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('continued_research', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['johnson.PlannedResearch'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('goal', self.gf('django.db.models.fields.TextField')()),
            ('effects', self.gf('django.db.models.fields.TextField')()),
            ('year', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('unit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Department'])),
        ))
        db.send_create_signal('johnson', ['PlannedResearch'])


    def backwards(self, orm):
        # Deleting model 'PlannedResearch'
        db.delete_table('johnson_plannedresearch')


    models = {
        'johnson.plannedresearch': {
            'Meta': {'object_name': 'PlannedResearch'},
            'continued_research': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['johnson.PlannedResearch']"}),
            'effects': ('django.db.models.fields.TextField', [], {}),
            'goal': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']"}),
            'year': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['johnson']