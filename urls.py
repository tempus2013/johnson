# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

urlpatterns = patterns('',
    (r'^$', 'apps.johnson.views.index'),

    (r'^plans$', 'apps.johnson.views.plans'),
    (r'^plans/staff', 'apps.johnson.views.plans_staff'),
    (r'^plans/research/add$', 'apps.johnson.views.plans_research_add'),
    (r'^plans/research/edit/(?P<research_id>\d+)$', 'apps.johnson.views.plans_research_edit'),
    (r'^plans/srd', 'apps.johnson.views.plans_srd'),

    (r'^reports$', 'apps.johnson.views.reports'),
)